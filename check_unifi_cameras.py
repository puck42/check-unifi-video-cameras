#!/usr/bin/python

"""
Check that a selected camera is connected to the server and set to record video.
For use with Ubiquiti's UniFi Video from version 3.9.9 onwards.

James Forman <james@jamesforman.co.nz>.

Version: 1.0.0
"""

import time
import datetime
import argparse
import sys
import requests

from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

COUNT_OK = 0
COUNT_WARNING = 0
COUNT_CRITICAL = 0
COUNT_UNKNOWN = 0
COUNT_IGNORED = 0
COUNT_TOTAL = 0

SERVER = "127.0.0.1"
PORT = 7443
IGNORE = "/etc/unifi-camera-ignore.txt"

if __name__ == "__main__":

    # Optional/Required arguements: http://bugs.python.org/issue9694
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--server', default=SERVER, help='Server to connect to, defaults to {0}'.format(SERVER))
    parser.add_argument('--port', default=PORT, type=int, help='Port to connect to, defaults to {0}'.format(PORT))
    parser.add_argument('--apikey', help='API Key to use, no default')
    parser.add_argument('--recording', default=False, help='State of recording, defaults to "False"')
    parser.add_argument('--debug', default=False, help='To print debug information set to "True"')
    parser.add_argument('--ignore', default=IGNORE, help='A file of UUIDs to ignore, defaults to {0}'.format(IGNORE))
    args = parser.parse_args()

    DEBUG = args.debug
    URL = "https://{0}:{1}/api/2.0/camera/?apiKey={2}".format(args.server, args.port, args.apikey)

    api_data = requests.get(URL, verify=False)
    json_data = (api_data.json())
    cameras = json_data["data"]

    for camera in cameras:
        if DEBUG:
            print("Evaluating camera with UUID: {0}".format(camera["uuid"]))
        COUNT_TOTAL = COUNT_TOTAL + 1

        try:
            if camera["uuid"] in open(args.ignore).read():
                if DEBUG:
                    print("Camera '{0}' ({1}) is in the ignore file".format(camera["name"], camera["model"]))
                COUNT_IGNORED = COUNT_IGNORED + 1
            else:

                if camera["state"] == "CONNECTED":
                    camera_uptime = int(time.time() - camera["uptime"] / 1000)

                    if args.recording is False:
                        if camera["recordingSettings"]["motionRecordEnabled"] is True:
                            if DEBUG:
                                print("Camera is set to do motion recording")
                        elif camera["recordingSettings"]["fullTimeRecordEnabled"] is True:
                            if DEBUG:
                                print("Camera is set to do full time recording")
                        else:
                            if DEBUG:
                                print("State: CRITICAL")
                            print("Camera '{0}' is not set to record".format(camera["name"]))
                            COUNT_CRITICAL = COUNT_CRITICAL + 1
                    elif args.recording == "motionRecord":
                        if camera["recordingSettings"]["motionRecordEnabled"] is False:
                            if DEBUG:
                                print("State: CRITICAL")
                            print("Camera '{0}' is not set to record on motion".format(camera["name"]))
                            COUNT_CRITICAL = COUNT_CRITICAL + 1
                    elif args.recording == "fullTimeRecord":
                        if camera["recordingSettings"]["fullTimeRecordEnabled"] is False:
                            if DEBUG:
                                print("State: CRITICAL")
                            print("Camera '{0}' is not set to record full time".format(camera["name"]))
                            COUNT_CRITICAL = COUNT_CRITICAL + 1
                    else:
                        if DEBUG:
                            print("State: CRITICAL")
                        print("Camera '{0}' is not set to record".format(camera["name"]))
                        COUNT_CRITICAL = COUNT_CRITICAL + 1

                    if camera_uptime <= 3600:
                        if DEBUG:
                            print("State: WARNING")
                        print("Camera '{0}' ({1}) has been connected for less than an hour".format(camera["name"], camera["model"]))
                        COUNT_WARNING = COUNT_WARNING + 1
                    else:
                        if DEBUG:
                            print("State: OK")
                            human_camera_uptime = str(datetime.timedelta(seconds=camera_uptime))
                            print("Camera: '{0}' ({1}) has been connected for {2}".format(camera["name"], camera["model"], human_camera_uptime))
                        COUNT_OK = COUNT_OK + 1

                else:
                    if DEBUG:
                        print("State: CRITICAL")
                    print("Camera: '{0}' ({1}) is not connected".format(camera["name"], camera["model"]))
                    COUNT_CRITICAL = COUNT_CRITICAL + 1

        except FileNotFoundError:
            if DEBUG:
                print("ERROR: No ignore file ({0}) found".format(args.ignore))
            raise

        if DEBUG:
            print("")

    if COUNT_IGNORED > 0:
        if DEBUG:
            print("{0}/{1} cameras were ignored".format(COUNT_IGNORED, COUNT_TOTAL))
        COUNT_TOTAL = COUNT_TOTAL - COUNT_IGNORED
    if COUNT_CRITICAL > 0:
        print("{0}/{1} cameras are in a CRITICAL state".format(COUNT_CRITICAL, COUNT_TOTAL))
    if COUNT_WARNING > 0:
        print("{0}/{1} cameras are in a WARNING state".format(COUNT_WARNING, COUNT_TOTAL))
    if COUNT_UNKNOWN > 0:
        print("{0}/{1} cameras are in an UNKNOWN state".format(COUNT_UNKNOWN, COUNT_TOTAL))
    if COUNT_OK > 0:
        print("{0}/{1} cameras are in an OK state".format(COUNT_OK, COUNT_TOTAL))

    if DEBUG:
        print("")

    if COUNT_CRITICAL > 0:
        if DEBUG:
            print("State: Critical")
        sys.exit(2)
    elif COUNT_WARNING > 0:
        if DEBUG:
            print("State: Warning")
        sys.exit(1)
    elif COUNT_UNKNOWN > 0:
        if DEBUG:
            print("State: Unknown")
        sys.exit(3)
    else:
        if DEBUG:
            print("State: OK")
        sys.exit(0)
