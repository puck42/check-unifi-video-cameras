# Purpose

This script is used to monitor all the cameras connected to a UniFi NVR via the NVR's API. It requires that:

* cameras are connected to the NVR
* cameras are set to record
* cameras have been connected for more than an hour

The script assumes that it will be run locally on the NVR so there's no need to validate the certificate.

# Requirements and Installation

## Requirements

* Python3
* Requests

## Installaton on Ubuntu

```bash
sudo apt update && sudo apt install python3 python3-requests
```

# How to use the script

By default the script will connect to https://127.0.0.1:7443/api/2.0/camera with a specified API key.

## Arguments

### Server (optional)
```bash
--server
```

The IP address or hostname of the UniFi NVR. The default is 127.0.0.1

### Port (optional)
```bash
--port
```

The port used by the UniFi NVR WebUI. The default is 7443

### API Key (required)
```bash
--apikey
```

The API key to authenticate to the UniFi NVR.

### Recording setting (optional)
```bash
--recording
```

The scripts defaults to both 'fullTimeRecord' and 'motionRecord', however by specifying either option it will only allow that option globally.

### Debug (optional)
```bash
--debug
```

To enable debug output set this option.

### Ignore file (optional)
```bash
--ignore
```

By default the script will look for a file that contains UUIDs of cameras to ignore in /etc/unifi-camera-ignore.txt. The script checks if a UUID exists anywhere in the file, so we can divide it and use it to temporarily or permanently disable checks of specific cameras.

This option allows you to specify a path to the a file that contains UUIDs of cameras to ignore.

An example ignore file is available as unifi-camera-ignore.txt

#### Ignoring specific cameras

Run the check with the debug flag set and get the appropriate UUID from the output:

```bash
Evaluating camera with UUID: e0e1e903-34c4-3e6f-92c1-88d4bd1f6308
State: CRITICAL
Camera: 'Side Alley' (UVC G3 Dome) is not connected
```

Add the UUID into the ignore file.

# Contributing
Please open an issue, create a new branch, complete your work in the branch and open a merge request.
